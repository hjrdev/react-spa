import React from 'react';
import { Link } from 'react-router-dom';

import { ActorList } from '../../data/actors.js'

import '../../styles/styles.css'

export default function Actors(props) {

  let actors = ActorList.map((actor) => {
    return (
      <div className="actor-container">
        <Link to={`/actors/${actor.url}`} alt="best actors oscar 2019">
          <div className="actor-image" style={{ backgroundImage: "url(" + actor.img_src + ")" }}></div>
        </Link>
        <h3>{actor.name}</h3>
      </div>
    )
  })

  return (
    <div className="main-content">
      <div><Link className="back" to="/">Back</Link></div>
      <h2>{props.title}</h2>
      <div className="container">
        {actors}
      </div>
    </div>
  );
}