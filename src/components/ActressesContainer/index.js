import React from 'react';
import { Route } from 'react-router-dom';

import Actress from '../Actress';
import Actresses from '../Actresses';
import { ActressList } from '../../data/actress';

export default function ActressesContainer() {

  let actressUrl = ActressList.map((actress) => {
    return (
      <Route path={`/actresses/${actress.url}`} render={() => <Actress name={actress.name} image={actress.profile_img} details={actress.description} />} />
    );
  })

  return (
    <>
      <Route exact path="/actresses" render={() => <Actresses title="Best Actresses" />} />
      {actressUrl}
    </>
  );
}
