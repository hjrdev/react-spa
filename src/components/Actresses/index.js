import React from 'react';
import { Link } from 'react-router-dom';

import { ActressList } from '../../data/actress.js'

import '../../styles/styles.css'

export default function Actresses(props) {

  let actresses = ActressList.map((actress) => {
    return (
      <div className="actor-container">
        <Link to={`/actresses/${actress.url}`} alt="best actresses oscar 2019">
          <div className="actor-image" style={{ backgroundImage: "url(" + actress.img_src + ")" }}></div>
        </Link>
        <h3>{actress.name}</h3>
      </div>
    )
  })

  return (
    <div className="main-content">
      <div><Link className="back" to="/">Back</Link></div>
      <h2>{props.title}</h2>
      <div className="container">
        {actresses}
      </div>
    </div>
  );
}