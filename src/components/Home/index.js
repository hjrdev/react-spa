import React from 'react';

import '../../styles/styles.css'

export default function Home(props) {
  return (
    <div className="main-content">
      <div className="container">
        <h2>{props.title}</h2>
        <div className="home-image"></div>
        <div className="thumbnail-container">
          <div className="home-thumbnail-1"></div>
          <div className="home-thumbnail-2"></div>
          <div className="home-thumbnail-3"></div>
        </div>
      </div>
    </div>
  );
}

