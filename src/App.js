import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom'

import NavBar from './components/NavBar';

import Home from './components/Home';
import ActorsContainer from './components/ActorsContainer';
import ActressesContainer from './components/ActressesContainer';
import Films from './components/Films';
 
import './styles/app.css'

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <NavBar title="Oscar 2019" />
        <Route path="/" exact render={() => <Home title="Oscar Winners" />} />
        <Route path="/actors" render={() => <ActorsContainer title="Best Actors" />} />
        <Route path="/actresses" render={() => <ActressesContainer title="Best Actress" />} />
        <Route path="/films" render={() => <Films title="The Best Films" />} />
      </div>
    </BrowserRouter>
  );
}

export default App;
